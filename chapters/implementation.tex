\SetAlgoSkip{}  % eat huge blank vskips surrounding algos in this chapter

\chapter{Implementation}
\label{chap:implementation}
The queue scheduling algorithm developed by the \ac{SOFIA}~\citep{Frank:2003} provides a well designed automated scheduling methodology.
\ac{SOFIA}, however, uses the \ac{DCSP} optimisation strategy.
This is a very good optimisation strategy, but is a greedy methodology that is computationally very time intensive.
For the proof-of-concept strawman implementation the simpler rank function introduced by the \emph{on-demand} scheduling strategy~\citep{Granzer:2004} and discussed in \cref{chap:autoschedule}, will be used as an alternative.

The basic queue scheduling problem can be stated as a permutation problem.
Every ordered list, $\{S(0), \cdots, S(N_s-1)\}$, of observations represents a possible solution and the optimum solution amongst them is found using some evaluation method~\citep{GomezdeCastro:2003}.
The goal of the strawman scheduler is to construct a good observation plan that can be queued and executed without much human interaction.
Generating this full observation plan is very time consuming since the system needs to evaluate all observations in a set of available observations \Observations, find those that can feasibly be scheduled at a given time \Starttime, identify the best observation from that subset and add it to the observation plan \Plan being built.
The time \Starttime is adjusted accordingly and the evaluation repeated as long as there are observations available to be queued.
In order to find the best queue sequence, the process is repeated for a number of permutations.

Note, in the algorithms below, the following notation applies:
%\vspace{-\parskip}
\begin{itemize}%[nosep]
  \item \EmptySet is the empty set, and
  \item $||$ indicates concatenation. % that an element is added to a set.
  This has the effect of including the observation in an unordered set (e.g.\ \Observations $||$ \Observation) or appending it to an observation plan, which is an ordered set (e.g.\ \Plan $||$ \Observation), or setting the start time of the next observation to be appended to an observation plan (e.g.\ \Plan $||$ \Starttime).
\end{itemize}

\section{Algorithm development}
\label{sec:algo}
The strawman algorithms were developed from the \ac{SOFIA} algorithms, but avoid the explicit use of flight-related parameters since these can be assigned more generally to extend the algorithms to ground based telescopes.

The fundamental algorithm of the \ac{SOFIA} scheduler is the \ForwardPlan algorithm~\citep{Frank:2003}.
The \ForwardPlan algorithm consists effectively of two sections: the first takes a list of possible take-off times and does a quick build of a short schedule for each start time.
It uses these short queues to find the best time to start the observation flight.
After this step completes, the queue construction part of the algorithm uses the chosen take-off time to build an optimal observation queue.
These two sections of the algorithm are completely independent and have been split up into separate functions for the strawman scheduler.

\begin{algorithm}
  \KwIn{%
    set of possible start times: \StartTimes \linebreak
    set of available observations: \Observations \linebreak
    look-ahead length: \Lookahead
  }
  \KwOut{%
    start time of plan: \Starttime
  }

  \BlankLine
  \Plan \Gets \EmptySet \;
  \WorkingSet \Gets \EmptySet \;

  \BlankLine
  \For{each start time \Starttime $\in$ \StartTimes}{
    \NewObservations \Gets \Observations \;
    \NewPlan \Gets \Plan $||$ \Starttime \;
    \NewPlan \Gets \LookAhead{\NewPlan, \NewObservations, \Lookahead} \;
    \WorkingSet \Gets \WorkingSet $\cup$ (\Starttime, \EvaluatePlan{\NewPlan}) \;
  }
  \If{\WorkingSet not empty}{
    \Starttime \Gets \Select{\WorkingSet}
  }

  \BlankLine
  \Return{\Starttime}

  \caption{StartTime}
  \label{alg:starttime}
\end{algorithm}

\StartTime, \cref{alg:starttime}, implements the initial section of the \ForwardPlan algorithm.
Although the \ac{SOFIA} implementation of this is specifically to identify a take-off time, this concept is also valuable for under-subscribed telescopes.
%By being able to inspect a range of times to start an observation, you can optimally distribute observations over the time available, instead of blindly scheduling them one after another, thus producing a sub-optimal schedule.
By being able to inspect a range of times to start observing, you can optimally distribute observations over the time available, instead of blindly scheduling targets as soon as they become visible, thus producing a sub-optimal schedule.

The \StartTime algorithm tries each observation that is feasible at the suggested start time, \Starttime, as the first observation and builds a short queue with length of the look-ahead length, \Lookahead, thus obtaining a per start time set of possible short queues.
The score from the highest scoring queue, \NewPlan, is selected and stored, along with the start time, \Starttime---which was used to generate this queue---in a score keeper list, \WorkingSet.
After scores have been obtained for each trial start time, the start time with the highest score is returned.

\begin{algorithm}
  \KwIn{%
    start time: \Starttime \linebreak
    set of available observations: \Observations \linebreak
    look-ahead length: \Lookahead
  }
  \KwOut{%
    observation plan: \Plan
  }
  
  \BlankLine
  \Plan \Gets \EmptySet \;
  \NewPlan \Gets \Plan $||$ \Starttime \;
  \While{\Observations not empty}{
    \WorkingSet \Gets \EmptySet \;
    \NewPlan \Gets \EmptySet \;
    \For{each observation \Observation $\in$ \Observations $-$ \Plan}{
      \If{\Feasible{\Observation}}{
        \NewPlan \Gets \Plan $||$ \Observation \;
        \NewObservations \Gets \Observations $||$ \Observation \;
        \NewPlan \Gets \LookAhead{\NewPlan, \NewObservations, \Lookahead} \;
        \WorkingSet \Gets \WorkingSet $\cup$ (\Observation, \EvaluatePlan{\NewPlan}) \;
      }
      \If{\WorkingSet not empty}{
        \NewObservation \Gets \Select{\WorkingSet} \;
        \Plan \Gets \Plan $||$ \NewObservation \;
        remove \NewObservation from \Observations
      }
    }
  }

  \BlankLine
  \Return{\Plan}
  
  \caption{ForwardPlan}
  \label{alg:forwardplan}
\end{algorithm}

Building the optimised queue is done in \ForwardPlan, \cref{alg:forwardplan}.
This algorithm takes each available observation, in turn, and does an exhaustive search over the remaining candidate observations until there is nothing feasible left to schedule.
The feasibility test also takes the remaining night length into account so that the algorithm terminates when no observations remain that fit into the remaining night length; that is to say, when the night has been fully scheduled.

In other words, starting at time \Starttime, returned from the \StartTime algorithm; select the next available observation as a candidate observation, \Observation; do an exhaustive search over all remaining unscheduled observations to obtain a number of short queues of look-ahead length, \Lookahead; select the highest scoring of these queues, \NewPlan, and add this queue, along with the candidate observation \Observation, which was used to generate it, to a set of candidate queues, \WorkingSet.
Repeat this process for all available observations to obtain a set of short queues and candidate observations.
From this set select the highest scoring queue and add the candidate observation, used to generate this queue, to plan \Plan.
Thus, plan, \Plan, is extended by repeating this next observation selection process until there are no more feasible observations left, thereby producing a highly optimised queue\footnote{It should be noted that the optimised queue generation step either ignores dynamic observation conditions or implements models such as predicted weather patterns.}.

\begin{algorithm}
  \KwIn{%
    observation plan: \Plan \linebreak
    set of available observations: \Observations \linebreak
    lookahead distance: \Lookahead
  }
  \KwOut{%
    observation plan extended by \Lookahead steps: \Plan
  }
  \Repeat{\Lookahead times}{
    \WorkingSet \Gets \EmptySet\;
    \For{each observation \Observation $\in$ \Observations $-$ \Plan}{
      \If{\Feasible{\Observation}}{
        Evaluate the rank function score of \Observation \;
        \WorkingSet \Gets \WorkingSet $\cup$ (\Observation, \Score{\Observation}) \;
      }
    }
    \If{\WorkingSet not empty}{
      \NewObservation \Gets \Select{\WorkingSet} \;
      \Plan \Gets \Plan $||$ \NewObservation \;
      remove \NewObservation from \Observations
    }
  }

  \BlankLine
  \Return{\Plan}

  \caption{LookAhead}
  \label{alg:lookahead}
\end{algorithm}

The generation of the short queues is the workhorse of the queue generation process.
The \LookAhead algorithm is given in \cref{alg:lookahead}.
It functions as follows: until the observation queue, \Plan, has been extended by the number of look-ahead steps, \Lookahead, and while there are unscheduled observations available, take a feasible observation and extend the queue by this observation and evaluate the queue's score in order to decide which observation provides the best scoring queue.
Do this for all feasible observations and select the observation that results in the highest queue score as the \emph{next} observation, \NewObservation.
The iterative cycle to select each next observation evaluates a selection over a number of steps into the future.
In other words, each cycle generates a throw-away schedule into the near future, for each candidate observation, to determine the best candidate to schedule by taking into account the possible observations that may follow it.


\section{Database}
\COMMENT{
Structural\slash geographical limitations are generally well set and also uses common engineering terminology.
On the other hand, Scientific terminology, such as pointing horizon and zones of avoidance, is related to the observation setup and output products.
Care should be taken during database design to avoid user confusion when setting these limits for the scheduler.
}

The algorithms described in \cref{sec:algo} form one of the pillars of the scheduler.
The other pillar is the database on which the algorithms operate.
The database contains all of the information on the targets merits and constraints.
In this section we describe the development and structure of the database.

The reason for looking at the database design is to verify that it is possible to store the observation targets, constraints, and related data in a manner that allows for easy and quick retrieval.
It is not hard to paint yourself into a corner with a restrictive design that does not leave options for future expansion, refinements and alterations.
Thus it is important that the storage solution should be general enough, and without being restrictive, so that it can cater for usage patterns in the future that cannot be foreseen during design time.
Note that this criterion holds specifically for the merit and veto parameters, as not all possible merits and vetoes may be known at design time.

This section explores a possible extensible storage layout that will try to allow future additions and alterations, specifically with regards to veto and merit parameters.
Of course this does not try to cater for the full database schema required the general operation of an observatory, or a single telescope for that matter; it simply focuses on the data relevant to scheduling observations.

The initial implementation, shown in \cref{fig:initial-db-model}, was structured with tables for: \texttt{target}, \texttt{block}, \texttt{merit}, \texttt{veto} and \texttt{properties}.
The \texttt{target} table holds the input parameters that define sky position as right ascension and declination.
It must be noted that the assumption is all target equatorial coordinates are astrometric J2000 catalogue positions.
To minimise duplication, identical targets are not repeated;
rather, targets are associated to projects, with individual projects assigned a unique programme identifier once a proposal has been accepted.
Following a modular design paradigm, the unique identifiers should be stored as part of the proposal management database which is not addressed in this development.
To tie the proposal to the observations, defined by the \texttt{block} table, a 1-to-many (or possibly a many-to-many) linking table---not included in the prototype database schema---is required.

\begin{figure}[hbt]
  \resizebox{\textwidth}{!}{
    \begin{tikzpicture}
      \pic{entity={block}{Block}{%
        id: & \textcolor{red}{(PK)} \\
        object\_id: & \textcolor{red}{(FK)} \\
        duration: & Integer \\
        active: & Boolean \\
        }{blue}};
      \pic[above=8ex of block] {entity={object}{Object}{%
        id: & \textcolor{red}{(PK)} \\
        ra: & Text \\
        dec: & Text \\
      }{blue}};
      \pic[right=7em of block] {entity={veto}{Veto}{%
        id: & \textcolor{red}{(PK)} \\
        veto\_name\_id: & \textcolor{red}{(FK)} \\
        property\_id: & \textcolor{red}{(FK)} \\
      }{red}};
      \pic[above=8ex of veto] {entity={vetoname}{VetoName}{%
        id: & \textcolor{red}{(PK)} \\
        name: & Text \\
      }{red}};
      \pic[left=7em of block] {entity={merit}{Merit}{%
        id: & \textcolor{red}{(PK)} \\
        merit\_name\_id: & \textcolor{red}{(FK)} \\
        property\_id: & \textcolor{red}{(FK)} \\
      }{green}};
      \pic[above=8ex of merit] {entity={meritname}{MeritName}{%
        id: & \textcolor{red}{(PK)} \\
        name: & Text \\
      }{green}};
      \pic[below=5ex of block] {entity={property}{Property}{%
        id: & \textcolor{red}{(PK)} \\
        property\_name\_id: & \textcolor{red}{(FK)} \\
        value: & Float \\
      }{brown}};
      \pic[below=8ex of property] {entity={propertyname}{PropertyName}{%
        id: & \textcolor{red}{(PK)} \\
        name: & Text \\
      }{brown}};
      \draw[many to many] (block.east) -- node[above, xshift=-2ex]{\footnotesize\texttt{contains}} (veto.west);
      \draw[many to many] (block.west) -- node[above, xshift=2ex]{\footnotesize\texttt{contains}} (merit.east);
      \draw[one to many] (object.south) -- node[right, yshift=-1ex]{\footnotesize\texttt{at}} (block.north);
      \draw[one to many] (vetoname.south) -- node[right, yshift=-1ex]{\footnotesize\texttt{named}} (veto.north);
      \draw[one to many] (meritname.south) -- node[right, yshift=-1ex]{\footnotesize\texttt{named}} (merit.north);
      \draw[one to many] (property.west) -| node[right, yshift=8.7ex]{\footnotesize\texttt{has}} (merit.south);
      \draw[one to many] (property.east) -| node[right, yshift=8.7ex]{\footnotesize\texttt{has}} (veto.south);
      \draw[one to many] (propertyname.north) -- node[right, yshift=1ex]{\footnotesize\texttt{named}} (property.south);
    \end{tikzpicture}
  }
  \caption[Initial database logical model]{Initial database logical model.}
  \label{fig:initial-db-model}
\end{figure}

Possible future expansion is to include targets under human friendly target names.
However, each catalogue defines a unique designation for each target object; designations for corresponding targets generally differ between catalogues.
Current design strategies for time-domain astronomy is to develop an ecosystem of telescopes with a central hub, housing relevant catalogues and target information across telescopes, forming part of the \ac{TOMS} network~\citep{Street:2018}.
Thus it would make sense to join this global effort and work to add a \ac{TOMS} \ac{API} as extension to the scheduler database at a future date. %; with this discussion of the database focused on the instrument observational requirements.

The \texttt{block} table defines an observation block and contains the observation parameters such as observation duration, priority, earliest start time and latest end time.
Blocks are used as the building elements of a schedule.

The reason the \texttt{target} is kept separate from \texttt{block}s is to prevent the duplication of data; this is a generally accepted design principle for relational database design.
Since the same target may conceivably be associated with multiple observations, repeating the target coordinates for each observation introduces the possibility of errors.
These errors may be caused when a particular value is initially entered incorrectly by the user, or by a target that may require adjustment, or refinement, at a later point in time.  Such a modification would then require multiple updates in multiple places, any of which may introduce fresh errors of their own.

In turn, the \texttt{merit} and \texttt{veto} tables hold the function references and strictness parameters of the observation specific constraints.
The \texttt{properties} table contains the values for each merit, or veto.
For example, the elongation veto needs a minimum distance value, as well as the name of the celestial object this distance relates to.

The central table is the \texttt{block}, with a 1-to-many relationship to the \texttt{target} and many-to-many relationships to the \texttt{merit} and \texttt{veto} tables.
The \texttt{merit} table has a 1-to-many relationship with the \texttt{merit-name} table, to prevent duplication of merit name strings, since any duplication brings about the possibility of inconsistencies, in this case, of merit name spellings or capitalisation.
A similar 1-to-many relationship is defined between the \texttt{veto} and \texttt{veto-name} tables.

Both the \texttt{merit} and \texttt{veto} tables have a 1-to-many relationship to the \texttt{properties} table, which in turn has a 1-to-many relationship to the \texttt{property-name} table.
Additionally, the \texttt{properties} table has a \emph{value} text column to hold the string representation of the applicable property value---the storage type of this column was chosen to be as generic as possible, as some values may be integers, while other values may be floating point numbers, and yet other values may be strings.

This sharing of the \texttt{properties} table by the \texttt{merit} and \texttt{veto} tables is not ideal as a specific property carries no indication of whether it contains a merit or a veto value.
An alternative to this is having a \texttt{merit-property} table and a \texttt{veto-property} table which have exactly the same structure; this may have been a better design decision, and may have been the next step in the iteration of this design, had it not been refined in a different way.

The use of a \texttt{properties} table had further disadvantages: parsing the property text strings proved hard.
The idea of instantiating an object given a textual representation sounds easy, but it introduces all kinds of special case treatments, which is not ideal where the type of the object isn't known at design time.
This means that all of the unique cases that might arise in the future cannot be catered for in a generic fashion.

The preceding discussion brings one in a roundabout fashion to think about storing metadata, or data about your data, in the database.
Thus, you have tables describing the data that can be found in other tables.
Generalising the idea produces a solution where data about other data is stored in the same table as the data, a so-called \ac{EAV} model.

Some research into the topic uncovered an online article, where \citet{Gorman:2006} recollects the rise and fall of a visionary \acs{IT} project he was involved in, which is a cautionary tale against the use of the \ac{EAV} model.
Further investigation led me to a blog post where \citet{Kyte:2009} talks about \emph{discovering} \ac{EAV}, and explains the reasons why not everybody is using it.
While reading through the comments to this post, I noticed that somebody had asked about using \ac{EAV} to address their unique storage problem, to which Kyte responded ``or just save it in a glob of XML'', meaning don't attempt to use the \ac{EAV} model, but rather store the data values as a string containing \ac{XML} in a given column in the database.
%I'm not a particularly big fan of \ac{XML} as I find it overly verbose, rather hard to read, and even harder to compose.

%However, there is a germ of an idea here;
Rather than using \ac{XML}, why not look at \ac{JSON} instead?  \ac{JSON} uses a rather less verbose notation, and while it is not as expressive as \ac{XML}, what it allows one to represent is quite sufficient for the purpose at hand.
Some digging into the matter brought the following to light: some databases handle data values encoded as \ac{JSON} natively, and can query the values directly from the \ac{JSON} key-value store---one does not need to query a particular value from the database and parse it in the application; the database does this for you.
Thus, the attributes can be stored as a \ac{JSON} string in a column in both the \texttt{merit} and \texttt{veto} tables.

%By using \ac{JSON}, one is better able to read and understand the contents, once properly formatted, as compared to \ac{XML}.
%The \ac{XML} representation can completely obfuscate the \emph{data} by the presence of its unnecessarily verbose structural markup.

\begin{figure}
  \resizebox{\textwidth}{!}{
    \begin{tikzpicture}
      \pic{entity={block}{Block}{%
        id: & \textcolor{red}{(PK)} \\
        object\_id: & \textcolor{red}{(FK)} \\
        duration: & Integer \\
        active: & Boolean \\
      }{blue}};
      \pic[above=8ex of block] {entity={object}{Object}{%
        id: & \textcolor{red}{(PK)} \\
        ra: & Text \\
        dec: & Text \\
      }{blue}};
      \pic[right=7em of block] {entity={veto}{Veto}{%
        id: & \textcolor{red}{(PK)} \\
        veto\_name\_id: & \textcolor{red}{(FK)} \\
        properties: & Text \\
      }{red}};
      \pic[above=8ex of veto] {entity={vetoname}{VetoName}{%
        id: & \textcolor{red}{(PK)} \\
        name: & Text \\
      }{red}};
      \pic[left=7em of block] {entity={merit}{Merit}{%
        id: & \textcolor{red}{(PK)} \\
        merit\_name\_id: & \textcolor{red}{(FK)} \\
        properties: & Text\\
      }{green}};
      \pic[above=8ex of merit] {entity={meritname}{MeritName}{%
        id: & \textcolor{red}{(PK)} \\
        name: & Text \\
      }{green}};
      \draw[many to many] (block.east) -- node[above, xshift=-2ex]{\footnotesize\texttt{contains}} (veto.west);
      \draw[many to many] (block.west) -- node[above, xshift=2ex]{\footnotesize\texttt{contains}} (merit.east);
      \draw[one to many] (object.south) -- node[right, yshift=-1ex]{\footnotesize\texttt{at}} (block.north);
      \draw[one to many] (vetoname.south) -- node[right, yshift=-1ex]{\footnotesize\texttt{named}} (veto.north);
      \draw[one to many] (meritname.south) -- node[right, yshift=-1ex]{\footnotesize\texttt{named}} (merit.north);
    \end{tikzpicture}
  }
  \caption[Database logical model]{Database logical model.}
  \label{fig:db-model}
\end{figure}

The final design is shown in \cref{fig:db-model}.
This design has drawbacks though: the \texttt{properties} column of the \texttt{merit} and \texttt{veto} tables may contain duplicate data between different merits and\slash or vetoes.
The design may be altered to have a \texttt{merit-property} and a \texttt{veto-property} table that contain the \ac{JSON} data for the merits and vetoes; a future iteration of the database design might explore this option.
A more serious drawback is that a particular \texttt{properties} entry may contain completely erroneous data that is not able to be parsed.
One will only realise the latter when one queries the particular erroneous merit or veto row, and tries to parse the data.
A way to guard against this possibility is to have a periodic process that selects each merit and veto individually, and verifies that the \ac{JSON} data is parseable.
Another way is to have a post-commit trigger in the database, which fails the commit if the data one tries to commit to a particular merit or veto is invalid; this would prevent erroneous data from making in into the database in the first place.
The strawman scheduler does not bother with this as the amount of data that is stored in the database is small, and the failure to parse data currently poses very little risk.

% \todo{add software process discussion here}