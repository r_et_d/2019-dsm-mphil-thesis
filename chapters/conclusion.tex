\chapter{Summary and future work}
\label{chap:conclusion}
Planning and scheduling generally refer to off-line processing, while observing requires good solutions with minimal computation time.
For each of these stages, we can define the problem input as consisting of the set of observations that have been requested, the constraints peculiar to the instrument\slash environment, and the optimisation of the objective function~\citep{Frank:2000}.
This dissertation presents a parametric scheduling strategy to achieve good time distributed observation queues that can be used as an initial scheduler for photometry observations on the \ac{ACT} telescope at Sutherland.

Maximum science efficiency is achieved by executing the programmes with highest scientific value first, under the required observing conditions.
Additionally, maximised scientific use of telescope time is obtained by having appropriate programmes ready for execution under a broad range of observing conditions.
The strawman implementation presented in this dissertation exploits one of the easier ways to generate good observation queues by optimising open shutter---on sky---time.
This approach simply requires proper time distribution of observations, weighted by scientific priority.
In addition, it needs quick and easy evaluation to compensate for queue breakage during observation time, by substituting better suited observations ``on the fly''.

Optimisation of choices is essential for astronomical observation scheduling and is achieved by representing constraints as merit functions with a strictness parameter associated to each.
The advantage of using parametric methods is that they are deterministic, which simplifies testing and verification since simple results are expected and results should always be the same for a given observational setup.
This helps to build confidence through the construction of schedule outcomes that are predictable given contrived targets specifically generated to allow the input to indicate what should be expected as the generated schedule.
In addition, as more constraints are added, they can be evaluated individually as simple single-merit constraints, as well as part of more expanded queues generated, making the implementation modular and easily adaptable.

% A note to the user is that implementation of deterministic methods require some randomness added to obtain more realistic queue generation.

Having proven the basic functionality of the scheduler, the logical next step will be to verify the queue generation process at a more scientific level.
This can be done by comparing computer-generated schedules with human-generated schedules of past observation nights from the \ac{ACT}.
In order to achieve comparable results some future work is required to extend the strawman implementation to allow non-consecutive observation scheduling.
By allowing some minimal amount of deadtime between observations, the scheduler will show a preference for selecting higher ranking observations.
Thus, by not only focusing on filling time immediately as an optimisation consideration, a science queue generation closer to the natural human evaluation results will be achieved.

Dynamic scheduling can also be introduced by randomly dropping an observation, or moving its location to a fixed observation position.
Overflow caused by this simulated meddling requires reworking of the permutations---thereby also influencing the off-line planning---since the induced over-subscription factor has to be absorbed as soon as possible.
