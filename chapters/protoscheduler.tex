%% developing a formalism for scheduling an optical telescope
\chapter[Scheduler]{Automated scheduler}
\label{chap:autoschedule}

Conceptually, a scheduler must take as input a set of observations that has been requested, as well as the constraints peculiar to the observations and specific of the instrument\slash environment~\citep{Frank:2000}.
The output will be some criteria derived from the optimisation of specified goals.
Some easy and fairly reliable methods to achieve this are described by \citet{Granzer:2004} and \citet{Frank:2003}.
This chapter will discuss the theoretical detail behind implementing these methods in the strawman scheduler.

As described in \cref{chap:introduction}, there are three criteria for a \emph{good schedule} 
\begin{enumerate*}[label = \emph{\alph*}),
                   before = \unskip{: },
                   itemjoin = {{, }},
                   itemjoin* = {{, and }},
                   after = \unskip{.}]
  \item fairness
  \item efficiency
  \item sensibility
\end{enumerate*}
A fair schedule balances time allocations between users such that they all share good and bad observing times equitably.
An efficient schedule is one that maximises instrument utilisation and strives to match observations with required conditions.
A sensible schedule is one that attempts only those observations that are possible under the current observing conditions~\citep{Denny:2004}.

These requirements for a good schedule are translated into observational constraints that can be evaluated during scheduling~\citep{Granzer:2004}.
Evaluation uses some optimisation of an objective function representing a per observation rank calculation based on the constraints~\citep{Frank:2003}.
It should be noted that an observation specifies both hard constraints and soft preferences.
The scheduling problem is to synthesise a schedule that satisfies all hard constraints and achieves a good score according to an objective function based on the soft preferences~\citep{Swanson:1994}.


\section{Basics of scheduling}
\subsection{Planning}
\label{subsec:plan}
The scheduling of astronomical observations is typically conducted on several different time scales.
Longer term planning deals with scheduling over the observation term given the approved science projects.
The main aim of this type of planning is the equitable distribution of time among users\slash partners, as well as maximising scientific return.
This phase only takes into account observational constraints that do not change, are known, or can be predicted\slash calculated very accurately.

Longer term planning deals with scheduling over the observation term, given the approved science projects.
The main aim of this section is the fair distribution of time among users\slash partners, as well as maximising scientific return.

Optimisation for long-term planning is mainly driven by the aims of the observatory and is restricted by the constraints of the telescope.
Observations can, and usually do, conflict.
Longer term plans allow for better resolution of these conflicts to achieve optimal scientific output.

For several reasons, it may not be possible to execute all approved projects within a given time frame.
Oversubscription is therefore permitted for the full cycle in order to ensure the complete use of available time.

Intermittent re-planning allows for the re-evaluation of observatory performance, which in turn allows for the re-evaluation of parameters or change of optimisation function.
Furthermore, over the lifetime of the observatory, other constraints may be required such as those imposed by a new instrumentation or by change in operations.

The optimisation strategy for planning ignores dynamic conditions and assumes
\begin{enumerate*}[label=\emph{\alph*}),
                   itemjoin={{, }},
                   itemjoin*={{, and }},
                   after = \unskip{~\citep{Denny:2004}.}]
  \item the problem-free execution of each observation
  \item perfect knowledge of the time duration needed for each observation
  \item perfect fore-knowledge of the weather throughout the night
\end{enumerate*}
Given the complexity and size of the search space for long-term scheduling, it is obvious that this type of scheduling cannot be done in real time; therefore the focus does not have to be on time-efficient algorithms.


\subsection{Scheduling}
\label{subsec:schedule}
Following the broader planning phase, the scheduling phase is more focused on optimising the use of the telescope, minimising overhead and maximising science output.

Setting up a dynamic queue of observations available for execution, based on a subset from the planning section, allows the scheduler to focus on efficient use of telescope time and instrumentation setup.
While planning decreases the number of observations to consider based on best-choice and other fixed constraints, setting up a selection of viable observations is subject to a large number of complex, heterogeneous constraints over both continuous and discrete variables.
Even relatively simple schedules have to deal with geometric constraints\COMMENT{ (if not fixed)}, precedence constraints, mutual exclusion constraints and temporal constraints, all in the same problem~\citep{Frank:2000}.

Non-scorable observations, such as \acp{TOO}, are subject to their own unique scheduling rules, where there is nothing to optimise.
The only goal is to ensure every required observation actually gets on the schedule given its individual constraints.
Optimisation of other observations happens around these observations and will generally result in a less-optimal solution.

Some observations are naturally more interesting to the science community than others.
However, due to the limited observation time, it may be necessary to observe a target many times, and so it may be more important to finish a sequence of observations on a given target rather than to start a new observation of another target.
In order to ensure maximum viable science output for publication an observation rule may state that once an observation is started it must be completed ahead of other observations still waiting to start, irrespective of rank~\citep{Frank:2000}.


\subsection{Observations}
\label{subsec:observing}
The scheduler thus generates a queue of observations available to be executed based on predicted values. %---observation plan\slash flight plan\slash etc.
These values are generally allowed to be oversubscribed with the system continually processing the short-term viability of queued observations during observation runs, based on constant updates that can include additional observations or additional constraints or triggered observations.

Executing observations is extremely time constrained and minimal optimisation should be done.
The emphasis is to ensure a balance between efficiency and sensibility.

Additional constraints may also come in the form of scheduling rules, which may in turn affect observation requirements.
An example is linked observations: once the first observation in a linked set is scheduled, the rest must be scheduled without optimisation of the individual observations if the entire set is to be completed in one round.

This requires on-demand scheduling strategies, where the scheduler dynamically makes a \emph{best} choice for the next observation, maximising science efficiency by executing the programmes with highest scientific value first and under the required observing conditions.
In addition, the scientific use of telescope time must be maximised by having appropriate programmes ready for execution under a broad range of observing conditions, thus being able to adapt to changing conditions, new requests, and acquisition errors, while still maintaining reasonable efficiency~\citep{Denny:2004}.


\section{The dispatch scheduler}
\label{subsec:granzer}
Operational parameters may be general to astronomy or unique to a telescope.
Other influences will depend on observatory policies and procedures such as those related to long-term projects, or compensations for time loss due to \ac{TOO} observations or similar programs.

In any queue scheduling methodology, the proper treatment of constraints on the observation is of paramount importance.
Some of these constraints are explicitly given by astronomers, while others are implicit, due to the nature of instrument\slash telescope~\citep{Frank:2000}.

In order to decide which observation, $n$, to carry out, a per observation objective function is evaluated~\citep{Steele:1997}:
\begin{equation}
  \label{eq:schedscore}
  R(n) = f(n)\cdot \prod\limits_{x=1}^{x=X}\upsilon_x(n) \cdot \frac{\sum\limits_{m=1}^{m=M}\varepsilon_m(n)}{M}
\end{equation}

For any observation constrained by $X$ hard limits and $M$ soft preferences: $f(n)$ is a measure of fairness, $\varepsilon_m(n)$ measures of efficiency and $\upsilon_x(n)$ Boolean veto functions as measures of sensibility.

Constraints are normalised to ensure an equal impact on the calculation from all, and to prevent a situation where high-valued constraints have a high impact, while low-valued, high-importance constraints have no real effect on the rank calculation.
Also, not all projects will have the same number of constraints and this must not unfairly bias some projects.
The only influence on selection must be scientific relevance~\citep{Maartens:2016}.

Observatory time must be shared equitably between projects.
The fairness function evaluates how equitable it is to perform a particular observation, based on the project's time allocation.
The time allocated to partners is thus a form of observatory accounting and when this drops below a partner's share of time, the system must give higher preference to that partner~\citep{Kubanek:2010}.

The veto function has to prevent observations being carried out that are not possible at the current time due to a number of Boolean constraints.
\begin{equation}
  \prod\limits_{x=1}^{x=X}\upsilon_x(n) = \upsilon_1(n)\cdot \upsilon_2(n)\cdot ...\cdot \upsilon_X(n) \, ,
\end{equation}
where $\upsilon_x(n)$ describes the constraint limits.

The purpose of the efficiency merits is to decide which observation to carry out, at any given moment in time, considering observatory policy, scientific importance and observing conditions~\citep{Steele:1997}.
\begin{equation}
  \sum\limits_{m=1}^{m=M}\varepsilon_m(n) = \beta_1 \varepsilon_1(n) + \beta_2 \varepsilon_2(n) + ... + \beta_M \varepsilon_M(n) \, ,
\end{equation}
where $\varepsilon_m(n)$ describes the constraint equations, each with an optional weighting factor $\beta_m$.


\subsection{Astronomical veto functions}
Astronomical constraints that can be considered as \emph{hard} constraints, are generally related to observational limits.

Positional fitness depends on the target position relative to some time standard and the observatory location.
One of the most obvious position conditions is target visibility.
In order to observe a target, it must be in a part of the sky visible to the telescope at some time during the observation period.

In terms of actual target sky visibility, the current definition will consider a target \emph{visible} if the target elevation is above the telescope's local horizon during the observation period.
\begin{equation}
  \upsilon(visible) = 1\, \forall\, \theta_\mathit{target} > \theta_\mathit{horizon} \in (N_\mathit{start}, N_\mathit{end})
\end{equation}
% eat vskip
\begin{function}[H]
  $N_\mathit{start} \gets$ observation period start time \;
  $N_\mathit{end} \gets$ observation period end time \;

  \BlankLine
  \eIf{$N_\mathit{start} \le$ target.minangle\_time \\
  \hspace{1em} \textbf{and} target.minangle\_time $\le N_\mathit{end}$ \\
  }{
    $\upsilon(\mathit{visible}) \gets \textbf{Permit}$ \;
  }{
  $\upsilon(\mathit{visible}) \gets \textbf{Prohibit}$ \;
  }
  \caption{Veto(visible)}
\end{function}

Target brightness evaluation is based on the instrument sensitivity limits related to the source target properties.
The brightness of the object must be low enough not to saturate the instrument, but high enough to provide a viable observation.

\begin{equation}
  \begin{array}{rcl}
    \mbox{instrument noise limit} & \leq &
    \mbox{Target brightness} \\
    & < & \mbox{instrument brightness limit} \\
    \multicolumn{3} {l} {\upsilon(magnitude) = 1 \in [\mbox{noise limit, brightness limit}]}
  \end{array}
\end{equation}
% eat vskip
\begin{function}[H]
  \eIf{instrument brightness limit $\le$ target brightness \\
  \hspace{1em} \textbf{or} target brightness $<$ instrument noise limit \\
  }{
    $\upsilon(\mathit{magnitude}) \gets \textbf{Prohibit}$ \;
  }{
    $\upsilon(\mathit{magnitude}) \gets \textbf{Permit}$ \;
  }
  \caption{Veto(magnitude)}
\end{function}
%\vspace{-1\topsep}% prevent bogus doubled paragraph break

Lunar phase and elevation not only influence sky brightness calculations, but also relates as a hard limit to observational \emph{brightness} conditions and can be defined in terms the percentage of the visible surface disc that is illuminated (\textsc{PLI}).
\begin{equation}
  \begin{array}{l}
    \mbox{Lunar brightness} = \left\{
    \begin{array}{l}
      \mbox{dark},\, \mbox{if PLI} < 0.4 \\
      \mbox{grey},\, \mbox{if PLI} < 0.7 \\
      \mbox{no constraint},\, \mbox{True}
    \end{array}
    \right . \\
    \\
    \begin{array}{lcl}
      \upsilon(\mathit{dark}) & = & 1,\,\mbox{if dark} \\
      \upsilon(\mathit{grey}) & = & 1,\,\mbox{if dark} \cup \mbox{grey} \\
      \upsilon(\mathit{any}) & = & 1,\,\mbox{if dark}^c \cap \mbox{grey}^c
    \end{array}
  \end{array}
\end{equation}

Note that the \textsc{PLI} values above are not mutually exclusive; targets that permit \emph{grey} time may also be scheduled during \emph{dark} time, for instance, in the event that no targets with a \emph{dark} requirement are available. 

%\vspace{-1.5\topsep}% prevent bogus doubled paragraph break
\begin{function}[H]
  \eIf{$\mathrm{dark\ PLI} <$ moon phase}{
    $\upsilon(\mathit{dark}) \gets \textbf{Prohibit}$ \;
  }{
    $\upsilon(\mathit{dark}) \gets \textbf{Permit}$ \;
  }
  \BlankLine
  \eIf{$\mathrm{grey\ PLI} <$ moon phase}{
    $\upsilon(\mathit{grey}) \gets \textbf{Prohibit}$ \;
  }{
    $\upsilon(\mathit{grey}) \gets \textbf{Permit}$ \;
  }
  \caption{Veto(sky brightness)}
\end{function}
%\vspace{-2\topsep}% prevent bogus doubled paragraph break

Conditions are considered to be photometric if the seeing is better than 1.3 arcseconds.
\begin{equation}
  \begin{array}{l}
    \mbox{Seeing} = \left\{
    \begin{array}{l}
      \mbox{poor},\, \mbox{if seeing} \ge \ang{;;1.3} \\
      \mbox{average},\, \ang{;;0.7} < \mbox{seeing} < \ang{;;1.3} \\
      \mbox{good},\, \mbox{if seeing} \le \ang{;;0.7}
    \end{array}
    \right . \\
  \end{array}
\end{equation}


\subsection{Astronomical efficiency functions}
General constraints are most important during the optimisation of the observation scheduling.
Since the \emph{strictness} of these soft preferences depends on the observation, soft constraints are defined using merit functions which can be adjusted to make the constraint more, or less, stringent.

\begin{enumerate}[label = (\alph*),
                  wide]
  \item \emph{Airmass merit}
  
  The closer a target is to the horizon, the more atmosphere the signal must pass through.
  Atmospheric absorption at lower elevations may cause degraded results.
  The general preference is to observe targets at as high elevation as possible.
  Airmass can be used to assign lower weights as the targets get closer to the horizon, thereby favouring observations at higher elevation.
  
  \begin{equation}
    \varepsilon_h(\mathit{airmass}) = \frac{1}{z(h)^\alpha}
  \end{equation}
  for the airmass at the observation reference position using the $\alpha$ coefficient to define the steepness of the merit (\cref{fig:airmass}).

  \begin{figure}[htb]
    \centering
    %\includegraphics[width=.75\linewidth]{images/airmass-merit}
    \input{images/airmass-merit.pgf}
    \caption[Plane-parallel airmass model]{Using the homogeneous plane-parallel atmosphere approximation, the airmass model is given by the secant of the zenith angle.
    The merit scales the strictness of the airmass model using steepness parameter $\alpha$.}
    \label{fig:airmass}
  \end{figure}

  \item \emph{Separation angle merit}
  
  The target must not at any stage of an observation approach within a specific minimum angular distance from the Moon.
  Separation angles may be dependent on the observation wavelength with different criteria between longer and shorter wavelengths, or brightness of target and comparator pair (\cref{fig:separation}).
  It is advised that the separation angle be chosen as narrow as possible since very strict phase and angle requirements may drastically reduce the time period in which the observation can be carried out, and hence a lower probability that it would be successfully completed.
  \begin{equation}
    \label{eq:separation}
    \varepsilon(\mathit{separation}) = \left(\frac{\theta(\mathit{target}, \mathit{Moon})-a}{b}\right)^c
  \end{equation}
  For a separation distance $\theta$ and separation limit $a$, parameters $b$ and $c$ are used to shape the strictness of the merit.

  \Cref{eq:separation} illustrates the separation merit for the Moon.
  Similar merits can be defined for other solar system bodies.

  \begin{figure}[htb]
    \centering
    %\includegraphics[width=.9\linewidth]{images/separation-merit}
    \input{images/separation-merit.pgf}
    \caption[Target separation angle merit]{Separation angles merit allows for softer limits as targets approach solar system objects, thus improving the probability of observation.}
    \label{fig:separation}
  \end{figure}

  \item \emph{Target altitude merit}
  
  When atmospheric effects are less important, but observations at higher altitude is still preferred due to mechanical or structural considerations; a simple piece-wise linear relation based on the altitude of the observation reference position can be used.
  \Cref{eq:altitude} provides such a calculation:
  \begin{equation}
     \label{eq:altitude}
     \varepsilon_a(\mathit{altitude}) = \frac{a - \max\{E_\mathit{min}(\mathit{horizon}), E_\mathit{min}(\alpha_\mathit{target}))\}}{\min\{E_\mathit{max}(\mathit{limits})\} - \max\{E_\mathit{min}(\mathit{horizon}), E_\mathit{min}(\alpha_\mathit{target})\}} \, ,
  \end{equation}
  and is graphically illustrated in \cref{fig:position}.

  \begin{figure}[htb]
    \centering
    %\includegraphics[width=.9\linewidth]{images/position-merit}
    \input{images/position-curve.pgf}
    \caption[Target altitude merit]{Simple position evaluation, piece-wise linear calculation over 30 minute intervals.
    The merit favours higher sky location during positional evaluation.}
    \label{fig:position}
  \end{figure}

  Parameters used in \cref{eq:altitude} are defined as $a$, the current altitude of the first target of the candidate observation, derived from the centroid over all linked observations in a linked sequence; \\
  $E_\mathit{min}(\mathit{horizon}) = w(a)$ for some observatory related horizon mask, $w$; \\
  $E_\mathit{min}(\alpha_\mathit{target})$ target visibility limit; \\
  $E_\mathit{max}(\mathit{limits}) < \mathit{zenith\ limit}$ with $\mathit{zenith\ limit}$ a singularity for alt\slash az mounts.

  For a southern hemisphere observatory at latitude, $\phi_\textrm{o}$ and an object with declination $\delta$, the minimum and maximum altitudes are calculated:
  \[
    E_\mathit{min}(\mathit{horizon}) = \left\{
    \begin{array}{l}
      \ang{-90}-(\phi_\textrm{o}-\delta),\, \mbox{if transit during observation period} \\
      \min\{E(N_\mathit{start}), E(N_\mathit{end})\}, \, \mbox{otherwise}
    \end{array}
    \right . \\, \,
  \]
  \[
    E_\mathit{max}(\mathit{horizon}) = \left\{
    \begin{array}{l}
      \ang{90}+(\phi_\textrm{o}-\delta),\, \mbox{if transit during observation period} \\
      \max\{E(N_\mathit{start}), E(N_\mathit{end})\}, \, \mbox{otherwise}
    \end{array}
    \right . \\, \,
  \]
  where $N_\mathit{start}$ ($N_\mathit{end}$) defines the start (end) of the observation period.

  \begin{function}[H]
    elevation$_\mathit{min}$ $\gets$ [ \\
      \hspace{2em} min(observer horizon($N_\mathit{start}$),
                   observer horizon($N_\mathit{end}$)), \\
      \hspace{2em} instrument minimum altitude, \\
      \hspace{2em} horizon mask(target azimuth) \\
    \hspace{1em} ] \;
    elevation$_\mathit{max}$ $\gets$ [ \\
      \hspace{2em} instrument zenith pointing limit, \\
      \hspace{2em} altitude(target transit) \\
    \hspace{1em} ] \;
    $\varepsilon(\mathit{target\ altitude}) \gets$ (target altitude $-$ max(elevation$_\mathit{min}$)) \\
    \hspace{1em} / (min(elevation$_\mathit{max}$) $-$ max(elevation$_\mathit{min}$)) \;
    \caption{Efficiency(target altitude)}
  \end{function}

  \item \emph{Rise and set time merit}
  
  In addition to the target position, timing related constraints are also very important for optimal scheduling.

  As the night progresses and targets rise, these targets become part of the scheduler options and must be evaluated depending on the strictness of starting observation at around the rise time, \cref{fig:boundary}. While, for setting targets a preference may be given to favour there observations closer to termination---shown in \cref{fig:window}.

  \begin{figure}[htb]
    \centering
    %\includegraphics[width=.9\linewidth]{images/set-boundary}
    \input{images/rise-boundary.pgf}
    \caption[Target rise time merit]{Merit indicating strictness to start observation around target rise time.}
    \label{fig:boundary}
  \end{figure}

  \begin{figure}[htb]
    \centering
    % \includegraphics[width=.9\linewidth]{images/target-window-boundary}
    \input{images/target-window-boundary.pgf}
    \caption[Target observation window merit]{A merit that increases the selection weight as the time remaining to observe the target in the current night decreases.}
    \label{fig:window}
  \end{figure}

  \begin{equation}
    \varepsilon(\mathit{boundary}) = 0.5\left[1-\tanh\left(s\frac{t-t_0}{\sigma}\right)\right]\, ,
  \end{equation}
  relaxed by the gradient $\sigma$ as time approaches the termination boundary and $t-t_0$.

  Together with evaluating setting targets, the window merit can also represent the evaluation of time remaining to complete observations for a given project.
  \Cref{fig:window} shows a window merit that increases the selection weight as the target observation window that can be used to evaluate both setting targets, as well as projects approaching completion, shortens~\citep{Granzer:2004}.
  \begin{equation}
    \varepsilon(\mathit{window}) = -a\times t_r + \left(\frac{b}{1+c\times t_r}\right)\, ,
  \end{equation}
  where $t_r = \frac{\Delta t_\mathit{target}}{\Delta t_\mathit{visible}}$ is the ratio of the target observation window over observation time remaining.
  The parameters $a,b,c$ in this merit are only used to control the steepness of the rise.
\end{enumerate}


\section{Putting it all together}
After identifying relevant parameters and describing their relation to the observation using the hard limit veto functions, as well as setting optimisation evaluation using the merit functions, an observation is ready to be scored using the objective function and can now be added to the scheduler.

For easy and continuous evaluation across all viable observations, the vetoes and merits must be structured in some logic algorithm; while, the data must be kept consistent with observations and accessible on demand.
Also, a memory of evaluation must be kept and non-viable observations removed.

All of which brings together the equations of \cref{chap:autoschedule}
into the implementation of \cref{chap:implementation}.
The strawman scheduler implementation addresses the problem of combining the mathematical constraints to distribute observations over time on a single instrument.
For the \ac{ACT} this means selecting a sequence of observations starting at some time after sunset, $N_\mathit{start}$, and ending before sunrise, $N_\mathit{end}$.