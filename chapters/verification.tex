\chapter{Testing and verification}
\label{chap:verification}
It is fairly obvious that computation time for the strawman scheduler is dependent on the number of observations that can make up a permutation.
However, it has the advantage of always producing an optimal queue and it is easy to analyse the sensitivity of the scheduler to any of the constraints.
Additionally, the scheduler introduces a fairness function to represent policy and procedure as well as scientific priority as part of the optimisation.
This chapter describes some of the basic verification used to evaluate the implementation as per the scheduling design and optimisation strategy presented in the previous chapters.

Fundamental to the scheduler is the score function presented in \cref{eq:schedscore}.
The definition of this score function parametrically represents the three major aspects of the scheduling strategy, namely fairness, efficiency and sensibility.
During the development of the strawman implementation, it is essential that verification tests be used throughout---not only to validate the correctness of the implementation, but also to ensure that the strategy of the score function is correctly captured in the Python implementation.

The strawman scheduler addresses the problem of distributing time on a single instrument.
For an optical telescope, this means selecting a sequence of observations starting at some time after sunset, $N_\mathit{start}$, and ending before sunrise, $N_\mathit{end}$.
This night length restriction provides an obvious choice for a first verification step.
The veto constraint validating target visibility determines whether an observation can be done, as well as the observation start time.
This hard constraint must prevent an observation to start before sunset or end after sunrise, which is a fundamental restriction to the \StartTime algorithm presented in \cref{alg:starttime}.
%First is to check the veto functions and ensure that observations do not violate any of the observational and instrumental restrictions.

Secondly, the strawman also focuses on off-line searches to obtain the best feasible queued solutions,
allowing the user to evaluate how these \emph{optimal} solutions are associated with the observations and deals with observational and site-specific constraints.
Where possible, evaluating individual merits will assist in verifying that the constraint is relevant to a typical observation.
Evaluating the effect of updates\slash changes to the merit strictness could indicate how a given parameter could impact the observation evaluation and thus its scheduling times.
By manipulating the expected impact on the observation schedule, the verification step tests the queue selection procedures implemented in the \ForwardPlan and \LookAhead algorithms of \cref{alg:forwardplan,alg:lookahead}.

Thirdly, the scientific priority of a program is a constant value that is assigned by the independent \ac{TAC} process.
As such it is not explicitly a merit function, but rather should be considered as an importance weight that should favour the more scientifically interesting observations.
That said, the priority parameter can be implemented both as part of the optimisation sum of the score function, or, alternatively as in \cref{eq:schedscore}, a definite weight affected only by time distribution fairness.
Evaluating both implementations during verification will help to identify which of the two will be the preferred option for optimal astronomy scheduling.

Finally, combining merits and the priority measure will show that the policies and procedures of the observatory can be met.

It should be noted that results obtained from the verification tests are evaluated by visually inspecting graphs showing per observation scoring in relation to optimal schedule score achieved, as shown in \cref{fig:case0planning}.
Also assessed is the observation distribution over the available night time for the selected observation schedule, \cref{fig:case0queue}.


\section{Test targets}
For the verification step the observation \emph{targets} are very contrived but this allows one to build trust in the implementation by ensuring the correct handling of the basic functionalities highlighted above.
Additionally, the selection of these targets makes visual verification, using displays of results over time, quick and easy.

The biggest issue is that the per observation evaluation, done by the scheduler, uses the target celestial coordinates to evaluate the scores over time and thus observation placement in the queue.
Yet, programmatically manipulating the observations to evaluate their updated locations in a queue is easiest if it could be structured as a time manipulation related to the nightly time line.

As a result, the test target generator was created for verification and acts as a translator that takes time offsets as inputs and constructs celestial targets for the scheduler to use.

The test source generator requires three values to generate a target for scheduling:%
\begin{itemize}[nosep]
  \item the target rise time with respect to sunset,
  \item the expected maximum elevation, and
  \item the observation duration.
\end{itemize}

Implicitly the function also uses values such as:
\vspace{-\parskip}
\begin{description}[nosep]
  \item[the date] that the targets need to be valid for, in essence the time the resulting schedule should be prepared for, as well as
  \item[the observer's location\textnormal{,}] or in other words, the geographical position of the observer: latitude, longitude, and elevation.
\end{description}

These implicit values are needed to construct the fake \emph{targets} that the scheduler will use to generate the observation queue.
Specifying the observer's position, the date and time, and the maximum elevation of the target, provides enough information to generate a \emph{celestial coordinate}.

The maximum elevation is the celestial object's elevation at meridian crossing.
The declination is simply $\delta = \pm \ang{90} \pm (\phi_\textrm{o} - A)$, where $A$ is azimuth, $\delta$ is the declination and $\phi_\textrm{o}$ is the observer's latitude.
It is easy to see that the maximum elevation is used here to fix the target's declination.

This is followed by calculating the compass direction the target will rise in using spherical trigonometry, $A =\cos^{-1}(\sin(\delta)/\cos(\phi_\textrm{o}))$.
At sunset the altitude angle is \ang{0}, giving an azimuth/altitude direction of $(A,0)$, which can then be converted to a \ac{RA}/\ac{Dec} coordinate pair by using an astronomy software package such as Ephem\footnote{\url{http://rhodesmill.org/pyephem/}}.
Thus, by knowing when sunset is on the day in question, and adding or subtracting the rise time from that, the target's \ac{RA} is obtained.

As an example, consider a target that rises half an hour before sunset on 16 March 2016, culminating at an altitude of \ang{65}, with an observer at \ac{SAAO} near the town of Sutherland.
The calculated celestial coordinates, that is the \ac{RA} $\alpha$ and \ac{Dec} $\delta$, of the target will be, ($\alpha$, $\delta$) = (\ra{14;05;07.28}, \ang{-7;18;09.4}).

The reader should take note that the calculation used to generate the verification targets will be invalid when it is presented with a circumpolar coordinate, or in other words, a target that does not cross the horizon.


\section{Basic functionality}
% The use of the simple test targets makes verification through visual evaluation an effective method to show correct functionality of the strawman scheduler.

The verification tests will start off evaluating behaviour using the culmination merit as the only constraint.
This is followed by the addition of other constraints to incrementally build a more complex observational environment.
The aim is to always evaluate the expected behaviour against the generated queue.
Test results will show the distribution of observations over time.
Using the target generator, targets will be constructed in a range of cases set up for easy evaluation.

The first test, \texttt{case 0}, has three targets that are well separated in \ac{HA}.
This ensures the targets have distinct culmination times.
Assuming all observations are of equal importance, the expectation is that scheduling the targets for observation at culmination will result in the targets simply following in sequence.
% All targets should have culmination locations close to the middle of the track.
Since the queue is evaluated only on the culmination merit it can be expected that the targets will be scheduled such that the observation time is close to the middle of the target track on sky.

The test targets are defined as follows: the first target rising three hours before sunset, the second target rising \num{130} minutes before sunset and the third target rising \num{40} minutes before sunset.
Targets culminate at elevations of \ang{65}, \ang{70} and \ang{55} respectively.
The duration of observation for the first and last targets will be an hour, while the second target will only be observed for 30 minutes.

\Cref{tb:case0} shows test target coordinates generated using the input time offsets and maximum elevation angles.

\begin{table}[htb]
  \centering
  \caption{Construction of simple sequential targets.}
  \label{tb:case0}
  \renewcommand{\arraystretch}{1.3}
  \begin{tabular}{ll}
    \hline
    \emph{a} & \verb|('10:34:35.03',  '-7:17:34.3')|, \\
    \emph{b} & \verb|('11:37:52.10', '-12:17:16.8')|, \\
    \emph{c} & \verb|('12:29:27.47',   '2:42:40.3')| \\
    \hline
  \end{tabular}
\end{table}

Using the fake input targets, the greedy algorithm of the scheduler will evaluate a range of observation sequence permutations.
Each observation will be allocated an individual score for its position in the queue being evaluated.
From this, the queue itself will be assigned a score for the generated observation plan.
The queue containing the observation plan with the highest score is then selected.

\begin{figure}[htb]
  \centering
  % \input{images/scores-case-0.pgf}
  \input{images/scores-case-1.pgf}
  \caption[Observation schedule for sequential targets]{Evaluating a series of observation queue permutations and selection of the optimal schedule for the sequential culmination observation of the three trial targets listed in \cref{tb:case0}.}
  \label{fig:case0planning}
\end{figure}

Visually evaluating the validity of the observation queue selected by the strawman scheduler is easiest if the queue is displayed as a nightly listing of observations, in order, over time\COMMENT{, similar to a Google calendar display}.
To achieve this \COMMENT{calendar} style of display, the queue permutations are shown as a bar graph with each observation represented as a block with length equal to the observation duration.
The colour of the block indicates the individual observation scores obtained, with the key to the right of the graph.
On the far right of the graphic, the respective overall observation scores are graphically depicted---a higher score is shown further to the right. \COMMENT{while a graph to the right of the bar graph shows the overall observation plan score.}
The sequence of observations scheduled in the queue is indicated by labelling the observations \emph{a}, \emph{b} and \emph{c} respectively.
\Cref{fig:case0planning} shows the scheduling results for \texttt{case 0}.
Note that the permutations, as shown here, follow in no specific order.

\begin{figure}[ht]
  \centering
  % \input{images/elevation-case-0.pgf}
  \input{images/elevation-case-1.pgf}
  \caption[Sequential target culmination queue verification]{Generated queue showing sequential targets, scheduled in order, starting from an estimated optimal start time and ensuring all targets are observed close to culmination.}
  \label{fig:case0queue}
\end{figure}

Once the best observation plan has been selected, the queue can be displayed as a function of elevation angle per target over time.
\Cref{fig:case0queue} shows the elevation plot with the sky track of each targets as a dotted curve, and the anticipated observation period for each target as a line overlaying the dotted curves. 
At the bottom of the curve a dot-dash line indicates the horizon-mask, set to an arbitrary constant value of \ang{20} altitude.
Below this, there is a shorter dotted line showing the range of start times the \StartTime algorithm has to its disposal to find the optimal start time for the observation plan.

Candidate schedule two has the highest queue score of the four permutations of queues shown in \cref{fig:case0planning} and is selected as the best observation plan to use.
The elevation plot in \cref{fig:case0queue} shows the target sequence of this queue and it is easily seen that all observation tracks are scheduled at highest elevation for the respective schedule period.

What is interesting is that although the score of the optimal queue, in \cref{fig:case0planning}, approaches the full score of 1, it is in fact not very close.
Expectation would be that, given the simple test setup, the score should be closer to, if not a full score.
Additionally, when inspecting \cref{fig:case0queue} more closely, it can be seen that the sequence of observations are not scheduled with each observation exactly over culmination.

\input{images/fig-case1queue.tex}

This highlights a shortcoming in the current implementation of the scheduler.
The current implementation of the scheduler is not sophisticated enough to deal with, or to allow, gaps in the scheduling time between target observations.
It simply optimises the fill time, or data acquisition factor.
This means that observations are scheduled as soon as they can be performed.
Changing the scheduling focus to rather maximise scientific output requires adding the ability to allow for non-observation time in order to find the optimal scheduling for the observation.
Allowing gaps between scheduled observations is planned as a refinement to be included in a future version of the software.
For the purposes of verification, we repeated this test with identical target observation durations\COMMENT{a more pleasing behaviour can be obtained by manipulating the target durations to better fit the observation period}, as shown in \cref{fig:case1queue}.
In this case the targets were all observed at culmination.
%setting the start time evaluation period long into the observation schedule time.
%Alternatively, by ensuring that there are enough observation targets to completely fill the entire observation schedule period will also guarantee schedule results closer to the expected behaviour for this test, as shown in \cref{fig:case1queue}.


Even though \cref{fig:case0queue} is not the \emph{optimal} outcome, it does clearly show that the observations are scheduled over maximum elevation given the restriction of not allowing time gaps between observations.
The strawman scheduler still generated a \emph{good} observation queue, %within
given the 
constraints, showing that the \ForwardPlan and \LookAhead algorithms do indeed calculate a valid observation queue.
It is however, very sensitive to the time period over which the best start time is selected, and will fail if there are observational time gaps between the targets to be scheduled.

Moving on to create a more complex setup, we include the case where observations compete for oversubscribed \ac{HA} ranges.
This is generally when the \ac{TAC} priority assignment is expected to influence the observation scores in such a way that the more scientifically interesting targets are favoured.

A very contrived test %\texttt{case 1}
case is constructed to best illustrate the impact of adding priority to the culmination constraint.
Instead of having three distinct targets, as in test \texttt{case 0}, the verification for test \texttt{case 1} uses a single target, namely  \verb|(`13:04:57.92', `-7:17:32.5')|.
The target now has to be scheduled three times, making impact evaluation easier given the expectation that the observations will simply be scheduled in sequence.

\begin{figure}[htb]
  \centering
  \input{images/elevation-case-2.pgf}
  \caption[Single target observed repeatedly]{Scheduling a single target multiple times, with a fixed period in which to evaluate the best start time to get an asymmetric observation queue.}
  \label{fig:case2queue}
\end{figure}

Test \texttt{case 1} sets the priority for all three observations to be equal.
When instructing the scheduler to only optimise for culmination and exploiting the fact that the scheduler will fill time starting as soon as possible, the observations are set up to obtain a queue where the last observation is over culmination, as shown in \cref{fig:case2queue}.
This asymmetry is necessary for visual inspection during the next step, since there is no clear way to distinguish the order in which the repetitions are scheduled.

Test \texttt{case 2} sets the priority of one the repetitions in test \texttt{case 1} to be higher than the other two.
This has no scientific meaning, but it will demonstrate the impact on the selected schedule, thereby providing a way to evaluate which of the two alternative implementations of the priority merit would be preferable: the priority merit included in the sum evaluation, or as part of the fairness weight.

\input{images/fig-case3-0-4queue.tex}

\Cref{fig:case3and4queue} shows the first implementation evaluating the scientific priority simply as part of the merit sum in the graph on the left.
The second implementation shows the outcome of the schedule evaluation when the priority is added as part of the fairness weighting shown in the graph on the right.
The resulting queue from the first implementation, \cref{fig:case3and4queue} left, is unexpected---the expected result was that both selected observation plans should look like \cref{fig:case3and4queue} right.
This result can be explained by looking at the score function definition of \cref{eq:schedscore}.
The merit sum implementation is calculated as an average to prevent artificially favouring observations as more merit constraints are added.
However, this also has another effect that is inherent to the averaging function, which is to raise values lower than average to be nearer to the average value, but also to lower values higher than the average to be closer to average.
When implementing the priority merit as part of the sum, a high priority at an optimal observation position could in fact result in an overall lower observational score, which is undesirable.

Consider the following simple example:
Let the culmination merits for the 3 sequential observations in \cref{fig:case2queue} be \emph{0.1}, \emph{0.5} and \emph{0.9} respectively.
Note, these values do not represent the actual values, but serve for illustration.
Secondly, let the priority weight assigned to the selected repetition be \emph{0.7}.
If the priority observation is assigned to the first observation position in the queue, the observation merit sum will update to \emph{0.4}, the average between \emph{0.1} and \emph{0.7}.
For the second and third repeats the values will be \emph{0.6} and \emph{0.8} respectively.
The important thing to note is that the observation that would be over culmination and thus having a high score, \emph{0.9}, will get a downward correction when the high priority is added to the average, \emph{0.8}.
Using these observation scores and calculating the per queue score for each of the permutations give the results listed in \cref{tb:case2}.

\begin{table}[htb]
  \centering
  \caption{Observation queue evaluation with priority in merit sum calculation.}
  \label{tb:case2}
  \renewcommand{\arraystretch}{1.3}
  \begin{tabular}{l}
    \hline
    sequence: a, b, b = \texttt{mean}$(0.4 + 0.5 + 0.9) = 0.600$\\
    sequence: b, a, b = \texttt{mean}$(0.1 + 0.6 + 0.9) = 0.533$\\
    sequence: b, b, a = \texttt{mean}$(0.1 + 0.5 + 0.8) = 0.467$\\
    \hline
  \end{tabular}
\end{table}

Thus, for the definition of the score evaluation given in \cref{eq:schedscore}, the more robust implementation of the priority is as part of the fairness weight outside the merit evaluation.

\input{images/fig-case5-5-1queue.tex}

Test \texttt{case 3} is a slight refinement on the previous test and considers two different targets, but with the same culmination time.
If no priorities are assigned the queue shows the targets observed in sequence, \cref{fig:case5queue}, which is similar to test \texttt{case 1} and the expected result.
While the expectation from test \texttt{case 2} is that increasing the priority of the target with higher elevation will cause the scheduler to flip the observation order, shown in \cref{fig:case5.1queue}.

\input{images/fig-case6-1-7queue.tex}

To simulate how incidental conditions, such as an observation being paused, will effect the schedule evaluation, we update test \texttt{case 0} such that the observation of the second target is paused and the programme tag for this condition will prevent the observation from being scheduled.
We then verify that the first and third target are scheduled just after and just before culmination since gaps are not allowed.
This is done both at high elevation and well positioned around culmination, \cref{fig:case6.1queue}.
For the repeating target setup, simply lower the number of repeats to only twice instead of three times.
The scheduler should now position the two observations of the target symmetrically around culmination, \cref{fig:case7queue}.
Note that for this case the start time evaluation period was extended to allow an observation schedule symmetrical about the culmination time.

% note: the figure on the left was manually edited to replace label 'b' with 'c'
\input{images/fig-culm-airm-comp.tex}

Lastly, the airmass merit should function similar to the culmination merit for the generated test targets since it will drive the observations to have targets with as high an elevation as possible.
\Cref{fig:case0-culm,fig:case0-airm} %and \cref{fig:case1-culm,fig:case1-airm}
show the elevation graphs scheduling the distinct observations of \texttt{case 0} using culmination, \cref{fig:case0-culm}, and airmass, \Cref{fig:case0-airm}.
%for \texttt{case 0} and \texttt{case 1}, respectively.
These figures compare the result of using the culmination merit compared to the airmass merit.
It should be noted that the figures of both cases are identical, as expected.
%% \Cref{fig:case0-culm-scores,fig:case0-airm-scores} and \cref{fig:case1-culm-scores,fig:case1-airm-scores} show the respective scores for the various candidate schedules.

% RvR: Remove the comment on scoring differences, the fact that the schedule looks the same is prob the only part the astronomer cares about. The differences in scoring will pop out in the long obs discussions
%% What is noticeable here is that even though the elevation graphs appear identical, the respective scores for the various candidate schedules that was evaluated consistently score lower for the airmass merit cases.
%% This difference in score values are attributable to the different functions used for the culmination merit as opposed to the airmass merit.
%% While the culmination merit uses a function that has a linear relation between the maximum elevation, with a score of 1 at the transit altitude, to the minimum elevation at the horizon, with a score of 0, the airmass merit follows an inverse $\cos$ function (that is to say, $\sec$) from the maximum value, directly overhead with a score of 1, to the minimum value of approximately $1/40$ at the horizon.



% \section{Multiple constraints}
\section{Observation scheduling}
Scheduling optical observations requires the scheduler not only to select relevant observations, but also to optimise open shutter time while filling the night length with viable observations.
Since the concept of \emph{night length} is relative to the seasons, the behaviour of the scheduler when building queues for longer (winter), versus shorter (summer) night durations must be validated as well.

To simulate and verify the behaviour of the strawman scheduler when building a queue for a full night of observation, ten targets are constructed to fill a nine-hour time period.
For easy visual evaluation the results will again only display the scores when considering culmination versus airmass.
The culmination merit will push the observation to be observed over highest elevation, while the airmass merit inherently tries to achieve the same, but by pushing the observations to be observed closer to zenith.
The biggest difference will be in the scoring evaluation.
While the culmination merit uses a function that has a linear relation between the maximum elevation, with a score of 1 at the transit altitude, to the minimum elevation at the horizon, with a score of 0, the airmass merit follows an inverse cosine function from zenith, with a score of 1, to the horizon value of approximately $1/40$.
The consequence is that while all targets will achieve a culmination merit of 1, targets with lower elevation culminations will always be assigned a low airmass score.
This highlights the need for correct merit selection, but also a need for the scheduler to be robust and not negatively impact the generated queue through introduced biased behaviour.
The difference in scheduling is graphically illustrated in \cref{fig:case9skyview,fig:case91skyview} and discussed below.
\input{images/fig-case9-9-1skyview.tex}

The simplest evaluation strategy to select the best queue is to pick the highest average over all per observation scores per queue permutation and to assign that permutation to be the optimal observation plan for the night.
When evaluating this best queue selection strategy for culmination, all targets are evaluated over the night and scheduled optimally as shown in \cref{fig:case9queue}.
% ,fig:case9skyview}.
However, as already highlighted, the airmass evaluation will optimise for zenith angle and thus favour targets that have higher culmination when evaluated using only airmass.
This results in an undesirable queue selection scheduling only the higher culmination targets later during the observation night, shown in \cref{fig:case91queue}.
%,fig:case91skyview}.
Again, the high culmination observations at the beginning of the night are lost, due to the current continuous observation time requirements for the scheduler optimisation.
\input{images/fig-case9-9-1.tex}

%\begin{figure}[ht]
%\centering
%\begin{minipage}[t]{0.49\textwidth}
%\centering
%\includegraphics[width=\textwidth]{bakimages/elevation-case-9.png}
%\caption[Observation night over culmination]{Observation night scheduling 10 observation over a 9 hour duration, optimised to observe targets at culmination.}
%\label{fig:case9queue}
%\end{minipage}
%\hfill
%\begin{minipage}[t]{0.49\textwidth}
%\centering
%\includegraphics[width=\textwidth]{bakimages/elevation-case-9_1.png}
%\caption[Observation schedule over airmass]{Observation night scheduling 10 observation over a 9 hour duration, optimised to observe targets at airmass. Using only observation score averaging to select the best queue}
%\label{fig:case91queue}
%\end{minipage}
%\begin{minipage}[t]{0.49\textwidth}
%\centering
%\includegraphics[width=\textwidth]{bakimages/skyview-case-9.png}
%\caption[Skyview showing culmination scheduling]{Polar plot showing the queue of targets all scheduled to be observed over culmination.}
%\label{fig:case9skyview}
%\end{minipage}
%\hfill
%\begin{minipage}[t]{0.49\textwidth}
%\centering
%\includegraphics[width=\textwidth]{bakimages/skyview-case-9_1.png}
%\caption[Skyview showing airmass scheduling]{Polar plot showing the queue of targets all scheduled to be observed over best airmass, thus closest to zenith.}
%\label{fig:case91skyview}
%\end{minipage}
%\end{figure}

Although all targets in \cref{fig:case91queue} are scheduled optimally, it is more desirable to obtain perhaps lower elevation observations, with targets throughout the night.
\Cref{fig:case91score} illustrates how the strawman scheduler selection strategy tries to find the highest scoring queue, while maximising the observation time during the night. Here the top, orange graph shows the calculated average over observations per queue permutation evaluated by the scheduler.
The centre blue graph shows the corresponding fill factor plotted per permutation.
When comparing the average scores per permutation to the fill factor, it becomes clear that the highest average scores coincide with minimal night coverage.
Consequently, the strawman scheduler weights the calculated average score with the night fill factor to ensure maximum coverage, even if at a lower calculated absolute queue score, as shown in the bottom green graph.
The importance being that the queue scores relative to each other, must be consistent and representative, rather than optimising the absolute score for any single merit.
This results in the airmass optimised queue shown in \cref{fig:case90queue}.
%\begin{figure}[ht]
%\centering
%\begin{minipage}[t]{0.49\textwidth}
%\centering
%\includegraphics[width=\textwidth]{bakimages/scores_fill-case-9_1.png}
%\caption[Queue evaluation scores]{Queue evaluation options as the average of the observation scores per queue permutation, top red graph. Queue evaluation by weighting the average, bottom green graph, with a fill factor, blue middle graph, to ensure full night scheduling.}
%\label{fig:case91score}
%\end{minipage}
%\hfill
%\begin{minipage}[t]{0.49\textwidth}
%\centering
%\includegraphics[width=\textwidth]{bakimages/elevation-case-9_0.png}
%\caption[Observation night over airmass]{Observation night scheduling 10 observation over a 9 hour duration, optimised to observe targets at airmass and ensuring maximum night coverage.}
%\label{fig:case90queue}
%\end{minipage}
%\end{figure}
\input{images/fig-case9-0-9-1score.tex}

Having validated the basic functionality of these merit functions, as well as queue selection evaluations, the last step is to ensure that the scheduler will behave properly over seasonal variations.
Both in terms of night length, as well as selected relevant targets to ensure only viable observations are queued.

Utilising the same set of 10 targets, the merit evaluations are selected to be either culmination or airmass on a random basis and evaluated for schedule at start time \verb|2016/3/16 18:24:46|.
This results in the observation plan presented in \cref{fig:case10queue}.
\begin{figure}[ht]
  \centering
  %\includegraphics[width=\textwidth]{bakimages/elevation-case-10_0.png}
  \input{images/elevation-case-10-0.pgf}
  \caption[Multi-target, multi-merit full night schedule]{Nine-hour observation plan for 10 targets with evaluation of either culmination or airmass, randomly selected. The resulting queue fills the observation night and schedules all targets to be observed at high elevation.}
  \label{fig:case10queue}
\end{figure}

To test the queue selection behaviour, the scheduler was set to evaluate queues at summer and winter solstice for the same set of candidate targets.
At summer solstice the night starts later and ends earlier, with a night length of only six hours.
Also, since the summer solstice is about 3 months before the date used for \cref{fig:case10queue}, the targets rise later.
For the same reason, the targets set earlier in winter solstice test case.
The schedule for the best summer solstice queue is shown in \cref{fig:case101queue}.
At winter solstice, the night starts earlier, and more targets are be visible and can be scheduled.
With a night length of 11 hours, the scheduler ran out of targets in the test case, \cref{fig:case102queue}.
\input{images/fig-case10-1-10-2queue.tex}

It is informative to show the scheduler evaluation for the summer solstice.
While five targets were found to be valid for observation over the summer night, only permutations of four targets at a time could be found to be valid over the short night duration, \cref{fig:case101score}.
\begin{figure}[ht]
  \centering
  %\includegraphics[width=\textwidth]{bakimages/scores_plus-case-10_1.png}
  \input{images/scores-plus-case-10-1.pgf}
  \caption[Queue permutations evaluated for summer]{Short summer nights limit the number of observations that can be scheduled given rise time, per target observation duration, as well as the requirements that all targets be observed at high elevation.}
  \label{fig:case101score}
\end{figure}

This chapter highlights how single merit validation tests ensured that the implementation represents the expected behaviour of a human observer successfully.
In addition, during the testing phase a number of implementation oversights and oversimplified assumptions has been identified and corrected, resulting in a fairly robust strawman scheduler.

%\begin{figure}[ht]
%\centering
%\begin{minipage}[t]{0.49\textwidth}
%\centering
%\includegraphics[width=\textwidth]{bakimages/elevation-case-10_1.png}
%\caption[Summer solstice queue]{Observation plan selected for 10 target list at summer solstice \texttt{2015/12/20 21:43:42}.}
%\label{fig:case101queue}
%\end{minipage}
%\hfill
%\begin{minipage}[t]{0.49\textwidth}
%\centering
%\includegraphics[width=\textwidth]{bakimages/elevation-case-10_2.png}
%\caption[Winter solstice queue]{Observation plan selected for 10 target list at summer solstice \texttt{2016/6/20 17:06:56}.}
%\label{fig:case102queue}
%\end{minipage}
%\end{figure}

% {\color{blue}
% Construct a list of seven targets with different maximum elevations and observation durations. Set the observation durations to fill an eight hour night length starting from sunset to ending at sunrise.
% Randomly select three and apply the culmination merit, while applying the airmass merit to the remainder.
% \todo[inline]{%
% 2 x 30mins \\
% 3 x 1.5h \\
% 2 x 1h
% }
% \todo[inline]{Depending on the target list generated, assign a high priority to the second or third target.}
% \todo[inline]{Generate the permutation scores and elevation plot of the best queue for this target list.}
% Astronomical queue generation will also be affected by the night length variation between seasons.
% Assuming the eight hour observation period is a long winter night, set sunset 1.5 hours later and sunrise 1.5 hours earlier to mimic the shortening night duration during summer.
% \todo[inline]{Using the same target list, generate a summer observation queue and discuss the results.}

% In addition, apply a window merit over the observation night length.
% This implements the observation start merit over a 30 minute period centred around the identified start time or the first target in the queue.
% As well as, a 10 minute period at the end of the night over which the observation end merit get applied to the last target in the queue.
% \todo[inline]{Define 30 minute region around the beginning and 10 minute period at the end of the night length to apply the start and end window merit.
% See how this affect the start time selection.}
% }