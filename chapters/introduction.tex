\chapter[Introduction]{Introduction}
\label{chap:introduction}
% - introductory chapter
%   - robotic telescopes
%       - ground based, also possibly discus space-based
%       - in general, then focus on specific telescope
%   - versus automatic telescopes
%   - schedulers
%       - astronomical
%       - other non-astronomical schedulers (areas where used)
%   ...
%   - constraints

% - thesis notes:
%     - ~6 chapters
%     - 20 p per chapter
%     - robotic telescopes: ~7 pages

%%  dearborne, bradford, atis syntax

Scheduling related to science instruments is typically more complex and quite different from standard, application-related scheduling problems which are routinely solved in industry.
This has largely to do with the uncertain exploratory nature of science.
Scheduling the use of scientific instruments requires making choices that impact other choices later, and involves many interacting complex constraints over both discrete and continuous variables.
Furthermore, sets of constraints are dependent on a given science project, while new types of constraints may be added as the fundamental problem changes~\citep{Frank:2000}.

The scheduling and acquisition of astronomy data is a multi-objective problem and can be broken into four sections
\begin{enumerate*}[label = \emph{\alph*}),
                   before = \unskip{: },
                   itemjoin = {{, }},
                   itemjoin* = {{ and }},
                   after = \unskip{~\citep{Denny:2004}.}]
  \item planning \item scheduling
  \item observing
  \item data management
\end{enumerate*}
The first three sections represent a strong interdependent problem generally referred to as \textsl{scheduling}.
Data management is only loosely related and feeds back into planning and scheduling through the contribution of previously acquired data to the assessment of progress toward the achievement of scientific goals and the scheduling of further observations to meet those goals.
%by evaluating the viability of the observation data to achieve the science goal which affects further observations and time.

%Given these complex, often inseparably connected constraints, astronomy scheduling requires long-term planning as well as short-term optimisation.  While one aspect of scheduling focuses on optimising resource utilisation as the goal, sky conditions can change significantly during an observing session, leading to schedule breakage.  In addition, rapidly changing scientific priorities may require prompt follow-up observations sometimes even arise during an observation session.  These issues give rise to the need for a scheduling system that is capable of recovering from periods of bad observational conditions and of integrating newly added observations during operation~\citep{GomezdeCastro:2003}.

%\pagebreak  % bump to next page
Astronomy projects are complex, often consisting of inseparably connected constraints, requiring long-term planning as well as short-term optimisation.
For observatories this translates into telescope scheduling that focuses on optimising resource utilisation as the goal, while contending with the reality that sky conditions can change significantly during an observing session, thereby causing breakage in pre-prepared queue schedules.
In addition, rapidly changing scientific priorities may require prompt, unplanned observations.
These issues give rise to the need for a scheduling system that is capable of recovering from periods of bad observational conditions, accommodating changing priorities, and integrating newly added observations during operation~\citep{Denny:2004}.

% This thesis presents a strawman scheduler that makes use of a pool of observations---rather than a queue---using a design strategy based on a modular implementation of the first three phases. This modular design~\citep{VanRooyen:2018} differentiates between planning, scheduling and observing activities in the following way: Planning not only concerns setting up an observation plan for a telescope and\slash or instrument, but also planning by the observatory on scheduling the observations to achieve some combination of operational and scientific objectives. Scheduling, on the other hand, requires planning information to assess temporal assignment of observations to best achieve an execution plan. Observing has to deal with the dynamic conditions during execution of an observation and best-choice selection based on available observation plans~\citep{Wall:1996}. The advantage of this modular design is that not all stages need to be implemented individually and can be combined based on the specific requirements of the telescope.

The challenge is to optimise the scientific return while maintaining good scheduler etiquette.
There are three criteria for a \emph{good} schedule
\begin{enumerate*}[label = \emph{\alph*}),
                   before = \unskip{: },
                   itemjoin = {{, }},
                   itemjoin* = {{, and }},
                   after = \unskip{.}]
  \item fairness
  \item efficiency
  \item sensibility
\end{enumerate*}
A fair schedule balances time allocations between users such that they all share \emph{good} and \emph{bad} observing times equitably.
An efficient schedule is one that maximises instrument utilisation and strives to match observations with required conditions.
A sensible schedule is one that attempts only those observations that are possible under the current observing conditions~\citep{Denny:2004}.
%However, any\slash all stages implemented should incorporate the criteria for good scheduling.

This dissertation presents a strawman scheduler that makes use of a dynamic queue of observations.
Using the merit implementation presented by \citet{Granzer:2004} which is used to approach the scheduling problem at \acs{STELLA}, paired with the algorithm presented by \citet{Frank:2003}, the scheduler allows for the dynamic conditions during execution of an observation and best-choice selection based on available observation plans~\citep{Wall:1996}.
% Providing an automated scheduling strategy for optical photometers such as the \acf{APT}.


\section{General introduction to the problem}
Observing time is a scarce resource \citep{Johnston:1988b} which is subject to the vagaries of the weather.
Fortunately not all astronomical observations require the very best atmospheric conditions, hence the need for planning and scheduling to take full advantage of the variations of the weather conditions \citep{GomezdeCastro:2003}.

The ultimate goal of scheduling is to maximise the scientific impact of the telescope.
It can be argued that the following goals contribute the science impact~\citep{Colome:2012}:
\begin{itemize}%[nosep]
  \item Minimising the telescope idle time;
  \item Minimising the time overheads due to the scheduler---in the case of dynamic scheduling;
  \item Maximising the time available for science observations;
  \item Maximising observations of the highest scientific priority; and
  \item Maximising the quality of the collected data, i.e.\ matching the observations' execution constraints to the execution conditions.
\end{itemize}

From the goals above, it follows that scheduling of astronomical observations is an example of a multi-objective problem, where different factors must be optimised.

This requirement for planning and scheduling is applicable in a wide range of sectors, from the chemical, petrochemical, and pharmaceutical industries, to waste management \citep{Verderame:2010}.
It falls into the NP-hard class of problems \citep{GomezdeCastro:2003}, where it is computationally infeasible to enumerate all of the possible permutations in order to select the optimal solution~\citep{Johnston:1989}; only a reasonable approximation of the optimal solution can be reached in a finite time.


\subsection{Observing strategies}

Most astronomical observatories employ one or more of the following modes of observation.
In the \emph{classical} mode of observing, an astronomer travels to the telescope for a predetermined length of time---typically in the order of one or more weeks---to observe their own targets for the duration of their allotted time.
An alternate to in situ observing is \emph{remote observing}, carried out by the astronomer via remote control from a site more convenient than the telescope itself.
A refinement of the traditional observing mode is \emph{service observing}, where on-site observing staff perform the observations based on specifications prepared by the astronomer.
It is noteworthy that service observing introduces the possibility to conduct multiple observing programmes concurrently \citep{Johnston:1988a}.
Service observing may itself be further refined into \emph{automated observing}, where the astronomer prepares the specifications of the observation and submits it for execution.
However, instead of on-site staff performing the observation, an automated telescope performs all the steps necessary to complete the observation.
As an even further refinement, \emph{robotic} observing is performed by telescopes which operate autonomously and use advanced \ac{AI} and \ac{ML} algorithms to select and schedule observations without any human interaction.


\subsection{Automated and robotic telescopes}

The automation of telescopes only really became a possibility with the advent of the microcomputer \citep{Genet:2011}.
Before the microcomputer was introduced, a few attempts at automating telescopes by controlling them remotely with a mainframe were made in the 1950s and 1960s, but this mostly proved to be unreliable \citep{CastroTirado:2010}.
In contrast, remote observing only became practicable with advances in network and communication technologies in the late-1980s \citep{Bresina:1994}, or in other words, with the advent of the Internet.

The evolution to telescope automation from remote to robotic can conceptually be summarised in the following categories---adapted from \citet{CastroTirado:2010}:
\begin{description}
  \item[Remotely operated telescope] A telescope system that performs remote observations following the request of an observer.
  Thus the observer instructs the remote telescope to perform various actions which are then subsequently carried out by the telescope.
  \item[Queue-scheduled automated telescope] A telescope that performs queued observations, without the immediate help of an observer.  The astronomer acts mostly in a supervisory role to react to schedule breakage or controlling incidental unscheduled observations.
  \item[Autonomous observatory] A telescope that performs remote observations and is able to adapt itself to changes in observing conditions or priorities during the task execution without any human assistance.
  This requires a sufficient level of situational awareness by the telescope, especially with regard to weather conditions.
  \item[Intelligent observatory] A robotic observatory in which decisions are taken by an \ac{AI} system.
  This implies that the telescope is in full control of selecting the optimum target to observe from the list of candidate targets.
\end{description}
Each level of automation builds upon the previous level; it is not possible to have a robotic intelligent observatory, for example, without the automation of all of the requisite components, or without the input from a weather monitoring system of some kind.

\subsection{Alan Cousins telescope}

Queue scheduling algorithms are widely used in astronomy~\citep{mora:2010} and we will use this approach to schedule the \ac{ACT} which is situated at the \ac{SAAO}, in Sutherland.
The \ac{ACT}, sometimes referred to as the \acf{APT}, is a 0.75-\si{\metre} automatic photoelectric telescope commissioned in mid-2000 \citep{Martinez:2002}.

\begin{figure}
  \centering
  \def\svgwidth{.9\columnwidth}
  \input{images/rsa-ct-suth.pdf_tex}
  \caption[Map of South Africa]{Map of South Africa, indicating the locations of Cape Town and Sutherland.}
\end{figure}

\begin{figure}
  \centering
  \begin{scaletikzpicturetowidth}{.95\textwidth}
    \begin{tikzpicture}[scale=\tikzscale]
      \clip (0,0) rectangle (10,6.18);
      \node at (1.45,4.2) {\includegraphics[width=4cm]{images/apt-dome.png}};
      \node at (8.8,1) {\includegraphics[height=4.5cm]{images/apt-base.png}};
      \node at (5.32,3.09) {\includegraphics[width=5cm,angle=-34,origin=c]
        {images/apt-struct.png}};
    \end{tikzpicture}
  \end{scaletikzpicturetowidth}
  \caption[Photographs showing the \acs{APT}]{Photographs of the \acf*{APT}, showing the dome housing the telescope (left), the telescope itself (centre), and some detail of the base of the telescope (right).
  %The confines of the dome prevented proper framing of the pictures.
  }
\end{figure}

When it was originally commissioned, the \ac{ACT} control system ran on two \textsmaller{\textsc{MS\-DOS~6.1}} \acp{PC}.
In 2010 the main control elements of the \ac{ACT} were upgraded~\citep{VanHeerden:2011} and now consists of a Linux based \ac{PC} and a \ac{PLC}.
The \ac{PC} contains cards that connect to the \ac{PMT}, acquisition system, telescope drives and the time system, and communicates with the \ac{PLC}.
Time is obtained from \ac{GPS} signals.
The \ac{PLC} controls the rest of the telescope functions such as the telescope focus, acquisition mirror motions, the filter wheels and the aperture wheel.
\Cref{fig:ACT-block} shows a schematic outline of the telescope system.

\begin{figure}[ht]
  \centering
  \begin{tikzpicture}[>=latex]
    \tikzset{
      nude/.style={text width=1cm, align=right},
      base/.style={draw, rectangle, text badly centered},
      lbox/.style={base, text width=3.25cm, text height=1.75ex, text depth=0.25ex},
      sbox/.style={base, text width=3cm},
      %dbox/.style={sbox, double},
      circ/.style={draw, circle, text badly centered, text width=2cm, double},
      object/.pic={
        \node[star, star points=5, star point ratio=1.65, draw] {};
      },
      photons/.pic={
        \foreach \s in {-0.2, 0, 0.2}
          \draw [
            xshift=\s cm, yshift=-\s cm,
            ->, decorate, decoration={snake, segment length=2mm, post length=1.4mm}
          ] (0.25, 0.25) to (-0.25, -0.25);
      },
      source/.pic={
        \pic at (.4,.4) {object};
        \pic at (0,0) {photons};
      }
    }

    \pgfdeclarelayer{edgelayer}
    \pgfdeclarelayer{nodelayer}
    \pgfsetlayers{edgelayer,nodelayer}

    \begin{pgfonlayer}{nodelayer}
      \node[lbox]   (0)                   {\strut{}\acs{PMT}};
      \node[lbox]   (1) [below=1ex of  0] {\strut{}Acquisition System};
      \node[lbox]   (2) [below=1ex of  1] {\strut{}Telescope Drives};
      \node[lbox]   (3) [below=3ex of  2] {\strut{}\acs{PMT} \acs{PSU}};
      \node[lbox]   (4) [below=1ex of  3] {\strut{}Apeture};
      \node[lbox]   (5) [below=1ex of  4] {\strut{}Filter Wheels};
      \node[lbox]   (6) [below=1ex of  5] {\strut{}Acquisition Mirror};
      \node[lbox]   (7) [below=1ex of  6] {\strut{}Dark Slide};
      \node[lbox]   (8) [below=1ex of  7] {\strut{}Hand Paddle};
      \node[lbox]   (9) [below=1ex of  8] {\strut{}Telescope Focus};
      \node[lbox]  (10) [below=1ex of  9] {\strut{}Pointing Encoders};
      \node[lbox]  (11) [below=1ex of 10] {\strut{}Dome Rotation};
      \node[lbox]  (12) [below=1ex of 11] {\strut{}Dome Dropout};
      \node[lbox]  (13) [below=1ex of 12] {\strut{}Dome Shutter};
      \pic[above right=5ex of 0.north]    {source};

      \node[circ] (pc)  [left=2cm of 1]   {\strut\acs{PC}\\};
      \node[circ] (plc) [left=2cm of 8]   {\strut\acs{PLC}\\};
      \node[sbox, rounded corners=5pt] (gps) [left=1.5cm of pc] {\strut{}Time Service (\acs{GPS})};

      \node [nude] (sig) [left=7cm of 13.west] {signal};
      \node [nude] (dat) [above=-0.5ex of sig] {data};

      \path (sig.east) +(.52cm,0) coordinate (sig_end);
      \path (dat.east) +(.52cm,0) coordinate (dat_end);
    \end{pgfonlayer}

    \begin{pgfonlayer}{edgelayer}
      \draw [ ->, dashed] (gps) to (pc);

      \draw [<->,       ] (pc) to (plc);

        \foreach \targ in {0,...,2}
        \draw [<->, dashed] (pc) to (\targ.west);

      \foreach \targ in {3,...,13}
        \draw [<->, dashed] (plc) to (\targ.west);

      \draw [ -         ] (dat)        -- (dat_end);
      \draw [ - , dashed] (sig)        to (sig_end);
    \end{pgfonlayer}
  \end{tikzpicture}
  \caption[\acs{APT} system block design]{\acl{APT} system block design~\citep{VanHeerden:2011}.}
  \label{fig:ACT-block}
\end{figure}

%\pagebreak  % bump to next page
Currently, the \ac{ACT} does not have dedicated weather sensors.
For weather information, it relies on data obtained from other facilities at the \ac{SAAO} in Sutherland, that publish their data either on the local intranet, or globally via the Internet.

The telescope system hibernates during daytime; at sunset the control computer opens the dome and performs the telescope's initialisation routines.
Then the telescope begins to work its way through a pre-prepared list of observing targets.
It steps through this list and for each target the telescope acquires the target using a pattern matching algorithm.
It then proceeds to perform observations of the target.
During the night's observation, a persistent pointing error may cause the system to re-initialise itself; if pointing cannot be reinitialised successfully---perhaps due to cloud obscuration of targets---the system shuts itself down and closes the dome to avoid damage to the telescope.
To protect the system, software and hardware limits restrict the movement of the telescope, protecting it from collisions with objects in the dome or from trying to reach unobtainable pointing positions.
The system shuts down again at morning astronomical twilight.
The observational data is recorded locally and transferred to the \ac{SAAO} offices in Cape Town for analysis during the course of the \COMMENT{next} day.

Ultimately the telescope is intended to be a fully robotic telescope with limited operational support needs.
Some advance toward this goal has been made by a full hardware interface to allow automated queue executions of observations \citep{VanHeerden:2011}.
The next phase is the implementation of an automated scheduler that will generate a queue of valid observations for each night of observation.
%The aim of this thesis is to implement such an automated scheduling strategy for the \ac{ACT} using relevant observation and instrumentation criteria and selected methodologies to optimise the delivery of the \ac{ACT} science objective.

The telescope was designed and built to execute a range of photometry programmes, but is used mainly for the long-term monitoring of variable stars.
In addition, there is the potential for \ac{TOO} observations in the form of unanticipated events, such as gamma ray bursts, and anticipated events such as occultations.
All photometric observations are restricted by the requirement of clear skies.
The current observation strategy is very much up to the assistant astronomer, who evaluates whether prevailing conditions are good enough to start observations, as well as when to end observations if the weather conditions deteriorate beyond what is needed for photometric observations.

The execution of schedule blocks cannot accommodate dynamically changing weather conditions, except for evaluating predicted conditions when generating the scheduling queue in the planning phase.
As a consequence, deciding whether the instrument will observe, or not, is considered to be outside the control-and-monitoring functionality of the system and is thus not used during planning.
The main scheduling constraints are thus observation parameters, instrument ability, and for monitoring type observations, observation time window constraints.


\section{Defining the project focus}
% Objective and scope of thesis

Selecting programs suitable to be observed from one observation run to the next can be as simple as an ordered list of observation blocks, using queue scheduling algorithms.
On the other hand, it may require some more intelligence: selecting from multiple, overlapping choices requiring Markov decision processes~\citep{Littman:1997}, or harnessing Genetic algorithms~\citep{Wall:1996} to build suitable sets of queues with the potential combinations of environmental and atmospheric conditions.

Various methods exist to select the \emph{best} possible candidate in the multitude of possible solutions of the intractable problem of observation scheduling.
The main distinction that can be made between the methods are between exact methods and heuristics~\citep{Buchner:2011}; exact methods include linear programming and constraint satisfaction problems, while heuristics include various \ac{AI} approaches, simulated annealing and evolutionary algorithms.

Dynamic\slash linear programming methods are greedy, brute-force techniques that are computationally expensive, but these methods have the advantage of \emph{always} finding a global optimal solution~\citep{Buchner:2011}.
Computational performance can be improved by selective and simplifying assumptions that are valid to the application, such as the case with \glsentrylong{DCSP}, and constraint optimisation problem methods~\citep{Frank:2003}.

Heuristic methods fall in the category of \ac{ML} algorithms, which include \ac{AI}, simulated annealing, and evolutionary algorithms~\citep{Buchner:2011}.
All these algorithms require training and upfront knowledge in order to make correct decisions.
This input generally comes from models generated by the observatory and is bootstrapped into the scheduling strategy.
The \acl{HST}~\citep{Johnston:1992} and the Liverpool Telescope are prime examples of such systems.
The Liverpool Telescope is notable in that it is currently the world's largest fully robotic terrestrial telescope.
It specialises in time domain astrophysics and has a dedicated instrument suite giving imagining, spectroscopic and polarimetric capabilities.~\citep{Fraser:2004}

The development and implementation of such extensive schedulers takes a lot of time and experience and is not fundamentally required to implement automated operation of the small \ac{ACT}.
For smaller telescopes the much more agile dynamic scheduling strategies have been shown to work very well.

%\pagebreak  % bump to next page
An appropriate scheduling strategy for the \ac{ACT} is \emph{dispatch}, or \emph{just-in-case}, sched\-uling.
While the scheduler fundamentally makes the best choice for the next observation it also implements simple models to pro-actively make the nominal schedule more robust.
This approach is able to adapt to changing conditions, new requests, and acquisition errors, while still maintaining a reasonable measure of efficiency
\citep{Denny:2004, Granzer:2004}.

The \ac{ACT} queue scheduler will combine the dynamic dispatch scheduling strategy with optimisation using the \acl{DCSP} optimisation~\citep{Frank:2003}---implemented by the \acf{SOFIA}.

%\ac{TOO} observations to be performed by the \ac{ACT} will assume that all the information needed to execute the observation will be provided on, or before, a set time on the day of observation.  This is to allow the daily scheduled queue to be updated before using it for observation that night.

%Astronomy data acquisition can be broken into four phases: planning, scheduling, observing and data management \citep{Denny:TheSocietyForAstronomicalSciences23RdAnnual:2004}.
%Of the four phases mentioned, the auto-scheduler tool will mainly act on the first three.
%The telescope control and monitoring, CAM, unit will be in control of the second and third, while the archive or pipeline systems are responsible for the last.

%Peripheral supplementary components, which are not included in the scheduler are: the proposal submission process driven by an observatory \ac{TAC} to select relevant observations; the tools associated with the accepted proposal submission in order to insert the required observation information into the observation database used by the scheduler; and the observation generator tools or the \ac{QA} processes\slash pipelines to evaluate the captured data.


\subsection{Outline of the dissertation}

\Cref{chap:background} introduces the reader to the scheduling problem by providing definitions for the variables generally playing a role in the scheduling of telescopes.

\Cref{chap:autoschedule} discusses the merits and constraints implemented for the \ac{ACT} queue scheduler.

\Cref{chap:implementation} and \Cref{chap:verification} present the strawman scheduler's implementation and verification, developed in Python, in some technical detail.

\Cref{chap:conclusion} rounds off the presentation of the strawman scheduler by summarising the scheduling technique implemented, as well as some discussion on generalising the implementation in future work.
% \Cref{chap:conclusion} rounds off the discussion of the scheduler with a conclusion, as well as some discussion on generalising the implementation.

% \Cref{chap:summary} summarises the scheduling technique and implementation presented.
